# Cross-product User Compatibility Pack

## What it is?

In v 2.10 of the Atlassian Shared Access Layer (SAL), the `UserKey` object was introduced to uniquely identify a user, and is immutable for the duration of the life of the user. All `UserManager` methods in SAL that references a user using the `username` has been deprecated and replaced by a similar method using the `UserKey`.

**Depending on the product, the `username` might change during the life of the user, so if you need a stable identifier, you must use the `UserKey`**

You can include this compatibility library in your plugin so that it can interact with products that comes with or without SAL v2.10+.

## When I need it?

* If you want to be compatible with older versions of the products not using SAL 2.10+ and any later products that uses SAL 2.10+
* With JIRA v 6.0's new feature that allows for usernames to be changed, this library will allow your plugin to be compatibility with JIRA 6.x and and 5.x

## How to use it?

The compatibility library has been designed so that it'll be easier to migrate from the old SAL library into using this. Likewise, it'll be easy to migrate away from the compatibility library when you no longer need it.

Add a maven dependency by adding following to your `pom.xml`:

    <dependency>
        <groupId>com.atlassian.usercompatibility</groupId>
        <artifactId>usercompatibility-sal</artifactId>
        <version>${usercompatibility.version}</version>
    </dependency>

And in your `atlassian-plugin.xml`, let Spring instantiate the `UserManager` as such:

    <component key="compatibilityUserManager" class="com.atlassian.sal.usercompatibility.impl.CompatibilityUserManager" />

Note that you should still have your SAL's `UserManager` context-imported in the `atlassian-plugin.xml`:

    <component-import key="userManager" interface="com.atlassian.sal.api.user.UserManager" />

The `CompatibilityUserManager` wraps the underlying implementation of the SAL `UserManager`, and uses that.
In your code, you can simply swap the use of the `com.atlassian.sal.api.user.UserManager` with the `com.atlassian.sal.usercompatibility.UserManager`.

For example:

    import com.atlassian.sal.api.user.UserManager;

    public class MyCode {
        private final UserManager userManager;

        public MyCode(UserManager userManager) {
            this.userManager = userManager;
        }

        public String getLoggedInUsername() {
            return userManager.getRemoteUsername();
        }
    }

will simply be replaced by:

    import com.atlassian.sal.usercompatibility.UserManager;

    public class MyCode {
        private final UserManager userManager;

        public MyCode(UserManager userManager) {
            this.userManager = userManager;
        }

        public String getLoggedInUsername() {
            return userManager.getRemoteUsername();
        }
    }
