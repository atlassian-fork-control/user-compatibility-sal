package com.atlassian.sal.usercompatibility.impl;

import java.net.URI;

import com.atlassian.sal.usercompatibility.UserKey;
import com.atlassian.sal.usercompatibility.UserManager;
import com.atlassian.sal.usercompatibility.UserProfile;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CompatibilityUserManagerWithUserKeyTest
{
    private final String USERNAME = "username";
    private final UserKey USERKEY = new UserKey("userkey");
    private final com.atlassian.sal.api.user.UserKey SAL_USERKEY = new com.atlassian.sal.api.user.UserKey("userkey");

    @Mock com.atlassian.sal.api.user.UserManager salUserManager;
    @Mock com.atlassian.sal.api.user.UserProfile salUserProfile;

    UserManager userManager;

    @Before
    public void setUp()
    {
        when(salUserProfile.getUserKey()).thenReturn(SAL_USERKEY);
        when(salUserProfile.getUsername()).thenReturn(USERNAME);
        when(salUserProfile.getFullName()).thenReturn("Full Name");
        when(salUserProfile.getEmail()).thenReturn("email@example.com");
        when(salUserProfile.getProfilePictureUri()).thenReturn(URI.create("/profile-pic"));
        when(salUserProfile.getProfilePageUri()).thenReturn(URI.create("/profile-page"));

        when(salUserManager.getUserProfile(USERNAME)).thenReturn(salUserProfile);
        when(salUserManager.getUserProfile(SAL_USERKEY)).thenReturn(salUserProfile);
        when(salUserManager.getUserProfile((com.atlassian.sal.api.user.UserKey) null)).thenReturn(null);
        when(salUserManager.getUserProfile((String) null)).thenReturn(null);
        when(salUserManager.getRemoteUserKey()).thenReturn(SAL_USERKEY);
        when(salUserManager.getRemoteUser()).thenReturn(salUserProfile);
        when(salUserManager.getRemoteUsername()).thenReturn(USERNAME);
        when(salUserManager.isUserInGroup(eq(SAL_USERKEY), anyString())).thenReturn(true);
        when(salUserManager.isUserInGroup(isNull(String.class), anyString())).thenReturn(false);
        when(salUserManager.isUserInGroup(isNull(com.atlassian.sal.api.user.UserKey.class), anyString())).thenReturn(false);
        when(salUserManager.isAdmin(USERNAME)).thenReturn(true);
        when(salUserManager.isAdmin((com.atlassian.sal.api.user.UserKey) null)).thenReturn(false);
        when(salUserManager.isAdmin((String) null)).thenReturn(false);
        when(salUserManager.isSystemAdmin(USERNAME)).thenReturn(true);
        when(salUserManager.isSystemAdmin((com.atlassian.sal.api.user.UserKey) null)).thenReturn(false);
        when(salUserManager.isSystemAdmin((String) null)).thenReturn(false);

        userManager = new CompatibilityUserManager(salUserManager);
    }

    @Test
    public void testGetRemoteUserKeyReturnsUsernameWhenSalUserKeyIsNotImplemented()
    {
        assertThat(userManager.getRemoteUserKey(), equalTo(USERKEY));
    }

    @Test
    public void verifyGetRemoteUserCallsSalRemoteUserWhenSalUserKeyIsImplemented()
    {
        userManager.getRemoteUser();
        verify(salUserManager).getRemoteUser();
    }

    @Test
    public void verifyGetRemoteUserDoesNotCallSalRemoteUsernameAndGetUserProfileWhenSalUserKeyIsImplemented()
    {
        userManager.getRemoteUser();
        verify(salUserManager, never()).getRemoteUsername();
        verify(salUserManager, never()).getUserProfile(USERNAME);
    }

    @Test
    public void verifyGetRemoteUserKeyCallsSalRemoteUserKeyWhenSalUserKeyIsImplemented()
    {
        userManager.getRemoteUserKey();
        verify(salUserManager).getRemoteUserKey();
    }

    @Test
    public void verifyGetRemoteUserKeyDoesNotCallSalRemoteUsernameWhenSalUserKeyIsImplemented()
    {
        userManager.getRemoteUserKey();
        verify(salUserManager, never()).getRemoteUsername();
    }

    @Test
    public void verifyGetProfileCallsSalUserProfileWhenSalUserKeyIsImplemented()
    {
        userManager.getUserProfile(USERKEY);
        verify(salUserManager).getUserProfile(SAL_USERKEY);
    }

    @Test
    public void verifyGetProfileCallsSalUserProfileByUsernameWhenSalUserKeyIsNotImplemented()
    {
        userManager.getUserProfile(USERKEY);
        verify(salUserManager, never()).getUserProfile(USERNAME);
    }

    @Test
    public void verifyGetProfileByUsernameCallsSalUserProfile()
    {
        userManager.getUserProfileByUsername(USERNAME);
        verify(salUserManager).getUserProfile(USERNAME);
    }

    @Test
    public void verifyIsUserInGroupCallsSalIsUserInGroupWithUserKey()
    {
        userManager.isUserInGroup(USERKEY, "group");
        verify(salUserManager).isUserInGroup(SAL_USERKEY, "group");
    }

    @Test
    public void verifyIsAdminCallsSalIsAdminWithUserKey()
    {
        userManager.isAdmin(USERKEY);
        verify(salUserManager).isAdmin(SAL_USERKEY);
    }

    @Test
    public void verifyIsSystemAdminCallsSalIsSystemAdminWithUserKey()
    {
        userManager.isSystemAdmin(USERKEY);
        verify(salUserManager).isSystemAdmin(SAL_USERKEY);
    }

    @Test
    public void verifyAuthenticateCallsSalAuthenticate()
    {
        userManager.authenticate(USERNAME, USERNAME);
        verify(salUserManager).authenticate(USERNAME, USERNAME);
    }

    @Test
    public void testGetUserProfileReturnsCorrectProfileFromSalUserManager()
    {
        UserProfile profile = userManager.getUserProfile(USERKEY);
        assertThat(profile.getUserKey(), equalTo(USERKEY));
        assertThat(profile.getUsername(), equalTo(USERNAME));
        assertThat(profile.getEmail(), equalTo("email@example.com"));
        assertThat(profile.getFullName(), equalTo("Full Name"));
        assertThat(profile.getProfilePageUri(), equalTo(URI.create("/profile-page")));
        assertThat(profile.getProfilePictureUri(), equalTo(URI.create("/profile-pic")));
    }

    @Test
    public void testGetUserProfileForNullUserKey()
    {
        UserProfile profile = userManager.getUserProfile(null);
        assertThat(profile, is(nullValue()));
    }

    @Test
    public void testUserInGroupForNullUserKey()
    {
        assertFalse(userManager.isUserInGroup(null, "group"));
    }

    @Test
    public void testIsAdminForNullUserKey()
    {
        assertFalse(userManager.isAdmin(null));
    }

    @Test
    public void testIsSystemAdminForNullUserKey()
    {
        assertFalse(userManager.isSystemAdmin(null));
    }
}
