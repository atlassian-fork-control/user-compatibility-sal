package com.atlassian.sal.usercompatibility;

import java.security.Principal;
import javax.servlet.http.HttpServletRequest;

import com.atlassian.sal.api.user.UserResolutionException;

/**
 * This wraps  SAL's {@link com.atlassian.sal.api.user.UserManager} to allow plugin developers to support product
 * version with and without the new UseyKey support (introduced in SAL v.2.10)
 */
public interface UserManager
{
    /**
     * Returns the profile of the currently logged in user or null if no user can be found.
     * <p/>
     * Getting the full {@link UserProfile} may have performance implications in some applications.
     * Consider using {@link #getRemoteUserKey()} if you don't need the full object.
     *
     * @return the {@link UserProfile} of the logged in user or null
     * @see #getRemoteUserKey()
     */
    UserProfile getRemoteUser();

    /**
     * Returns the key of the currently logged in user or null if no user can be found.
     *
     * @return the {@link UserKey} of the logged in user or null
     * @see #getRemoteUser()
     */
    UserKey getRemoteUserKey();

    /**
     * Returns the username of the currently logged in user or null if no user can be found.
     * <p/>
     * Getting the full {@link UserProfile} may have performance implications in some applications.
     * Consider using {@link #getRemoteUserKey()} if you don't need the full object.
     *
     * @param request The request to retrieve the username from
     * @return the {@link UserProfile} of the logged in user or null
     * @see #getRemoteUser()
     * @see #getRemoteUserKey(javax.servlet.http.HttpServletRequest)
     */
    UserProfile getRemoteUser(HttpServletRequest request);

    /**
     * Returns the key of the currently logged in user or null if no user can be found.
     *
     * @param request The request to retrieve the username from
     * @return The key of the currently logged in user or null
     * @see #getRemoteUserKey()
     * @see #getRemoteUser(javax.servlet.http.HttpServletRequest)
     */
    UserKey getRemoteUserKey(HttpServletRequest request);

    /**
     * Returns a {@code UserProfile object} for the specified user or null if no user can be found
     * @param userKey The userKey of the user whose profile is requested
     * @return The user's profile or null
     */
    UserProfile getUserProfile(UserKey userKey);

    /**
     * Returns a {@code UserProfile object} for the specified user or null if no user can be found
     * @param username The username of the user whose profile is requested
     * @return The user's profile or null
     */
    UserProfile getUserProfileByUsername(String username);

    /**
     * Returns whether the given user is in the given group
     *
     * @param userKey  The user
     * @param group    The group
     * @return {@code true} if the user is in the specified group
     */
    boolean isUserInGroup(UserKey userKey, String group);

    /**
     * Returns {@code true} or {@code false} depending on whether a user has been granted the system administrator
     * permission. A system administrator has full administrative permissions in the application, including permission
     * to perform operations that may affect the underlying operating system, such as specifying filesystem paths,
     * installing plugins, configuring mail servers and logging, performing backups and restores, etc. Only check for
     * system administrator when performing this type of operation. Operations that do not affect the underlying system
     * should use {@link #isAdmin(UserKey)} instead.
     *
     * @param userKey The key of the user to check
     * @return {@code true} or {@code false} depending on whether a user has been granted the system admin permission.
     * @see <a href="http://confluence.atlassian.com/display/JIRA/Managing+Global+Permissions#ManagingGlobalPermissions-About%27JIRASystemAdministrators%27and%27JIRAAdministrators%27">About 'JIRA System Administrators' and 'JIRA Administrators'</a>
     * @see <a href="http://confluence.atlassian.com/display/DOC/Global+Permissions+Overview#GlobalPermissionsOverview-confluenceadmin">Comparing the System Administrator with the Confluence Administrator Permission</a>
     */
    boolean isSystemAdmin(UserKey userKey);

    /**
     * Returns {@code true} or {@code false} depending on whether a user has been granted the administrator permission.
     * An administrator may have restricted administrative permissions that only apply to application-level
     * configuration that cannot affect the underlying operating system. Only check for administrator permission when
     * performing this type of operation. Operations that can affect security, the filesystem, or allow arbitrary code
     * execution must check {@link #isSystemAdmin(UserKey)} instead.
     * <p/>
     * Note that system administrator permission implies administrator permission. That is, any username for which
     * {@code userManager.isSystemAdmin(username)} returns {@code true} will also return {@code true} for
     * {@code userManager.isAdmin(username)}.
     *
     * @param userKey The user of the user to check
     * @return {@code true} or {@code false} depending on whether the user has been granted the admin permission
     * @see <a href="http://confluence.atlassian.com/display/JIRA/Managing+Global+Permissions#ManagingGlobalPermissions-About%27JIRASystemAdministrators%27and%27JIRAAdministrators%27">About 'JIRA System Administrators' and 'JIRA Administrators'</a>
     * @see <a href="http://confluence.atlassian.com/display/DOC/Global+Permissions+Overview#GlobalPermissionsOverview-confluenceadmin">Comparing the System Administrator with the Confluence Administrator Permission</a>
     */
    boolean isAdmin(UserKey userKey);

    /**
     * @see {@link com.atlassian.sal.api.user.UserManager#authenticate(String, String)}
     */
    boolean authenticate(String username, String password);

    /**
     * @see {@link com.atlassian.sal.api.user.UserManager#resolve(String)}
     */
    Principal resolve(String username) throws UserResolutionException;
}
