package com.atlassian.sal.usercompatibility;

import java.net.URI;

/**
 * This class wraps SAL's {@link com.atlassian.sal.api.user.UserProfile} in a way that is compatible with
 * products with or without SAL v.2.10+
 *
 * In SAL versions before 2.10, the user key is the lower-cased username.
 */
public interface UserProfile
{
    /**
     * Returns the key of the user associated with this profile information.
     *
     * Note: the key is meant to uniquely identify a user, and be immutable for the duration of the life of the user. It
     * is however not meant to be displayed: please use {@link #getUsername()} or {@link #getFullName()} for that purpose.
     *
     * @return the key of the user associated with this profile information.
     * @see {@link com.atlassian.sal.api.user.UserProfile}
     */
    UserKey getUserKey();

    /**
     * @see {@link com.atlassian.sal.api.user.UserProfile#getUsername()}
     */
    String getUsername();

    /**
     * @see {@link com.atlassian.sal.api.user.UserProfile#getFullName()}
     */
    String getFullName();

    /**
     * @see {@link com.atlassian.sal.api.user.UserProfile#getEmail()}
     */
    String getEmail();

    /**
     * @see {@link com.atlassian.sal.api.user.UserProfile#getProfilePictureUri(int, int)}
     */
    URI getProfilePictureUri(int width, int height);

    /**
     * @see {@link com.atlassian.sal.api.user.UserProfile#getProfilePictureUri()}
     */
    URI getProfilePictureUri();

    /**
     * @see {@link com.atlassian.sal.api.user.UserProfile#getProfilePageUri()}
     */
    URI getProfilePageUri();
}
