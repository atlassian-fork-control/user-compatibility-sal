package com.atlassian.sal.usercompatibility.impl;

import java.lang.reflect.Method;
import java.net.URI;

import com.atlassian.sal.usercompatibility.CompatibilityUserUtilAccessException;
import com.atlassian.sal.usercompatibility.UserKey;
import com.atlassian.sal.usercompatibility.UserProfile;

import static com.atlassian.sal.usercompatibility.UserKeys.getUserKeyFrom;
import static com.atlassian.sal.usercompatibility.UserKeys.isUserKeyImplemented;
import static com.google.common.base.Preconditions.checkNotNull;

class CompatibilityUserProfile implements UserProfile
{
    private final com.atlassian.sal.api.user.UserProfile userProfile;

    CompatibilityUserProfile(com.atlassian.sal.api.user.UserProfile userProfile)
    {
        this.userProfile = checkNotNull(userProfile, "userProfile");
    }

    @Override
    public UserKey getUserKey()
    {
        if (isUserKeyImplemented())
        {
            Class<?> userProfileClass = com.atlassian.sal.api.user.UserProfile.class;
            try
            {
                Method method = userProfileClass.getMethod("getUserKey");
                return new UserKey(method.invoke(userProfile).toString());
            }
            catch (Exception e)
            {
                throw new CompatibilityUserUtilAccessException(e);
            }
        }
        return getUserKeyFrom(userProfile.getUsername());
    }

    @Override
    public String getUsername()
    {
        return userProfile.getUsername();
    }

    @Override
    public String getFullName()
    {
        return userProfile.getFullName();
    }

    @Override
    public String getEmail()
    {
        return userProfile.getEmail();
    }

    @Override
    public URI getProfilePictureUri(int width, int height)
    {
        return userProfile.getProfilePictureUri(width, height);
    }

    @Override
    public URI getProfilePictureUri()
    {
        return userProfile.getProfilePictureUri();
    }

    @Override
    public URI getProfilePageUri()
    {
        return userProfile.getProfilePageUri();
    }
}
